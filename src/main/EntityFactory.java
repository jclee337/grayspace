package main;

import com.artemis.Component;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.utils.Bag;
import com.artemis.utils.ImmutableBag;
import main.components.*;
import main.serialization.EntityConstructor;
import main.serialization.EntityRepresenter;
import main.systems.PhysicsSystem;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;

import java.io.*;
import java.net.URL;
import java.util.*;

public class EntityFactory {
  private static HashMap<String, Entity> prototypes = new HashMap<String, Entity>();
  private static HashMap<String, String> serials = new HashMap<String, String>();
  private static World protoWorld;
  private static PhysicsSystem protoPhysics;

  public static void loadPrototypes() {
    protoWorld = new World();
    protoPhysics = new PhysicsSystem();
    protoWorld.setManager(new GroupManager());
    protoWorld.setManager(new TagManager());
    protoWorld.setManager(new TypeManager());
    protoWorld.setManager(new EventManager());
    protoWorld.setManager(new PoolManager());
    Yaml yaml = new Yaml(new EntityConstructor(protoWorld, protoPhysics));
    URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
    File file = new File(url.getPath());
    String path = file.getParent().replace('\\', '/') + "/yaml";
    File dir = new File(path);
    File[] directoryListing = dir.listFiles();
    if(directoryListing != null) {
      for(File child : directoryListing) {
        if(!child.getName().endsWith(".yml") || child.isDirectory())
          continue;

        try {
          String serial = new Scanner(child).useDelimiter("\\A").next();
          Entity prototype = (Entity)yaml.load(serial);
          prototype.disable();
          prototype.changedInWorld();
          String type = protoWorld.getManager(TypeManager.class).getType(prototype);
          serials.put(type, serial);
          prototypes.put(type, prototype);
        }
        catch (FileNotFoundException e) { e.printStackTrace(); }
      }
    }
  }

  public static Entity getEntity(String type, World world, PhysicsSystem physics) {
    Entity entity = world.getManager(PoolManager.class).getDisabledEntity(type);
    if(entity == null)
      entity = loadEntity(type, world, physics);
    return entity;
  }

  public static Entity loadEntity(String type, World world, PhysicsSystem physics) {
    Yaml yaml = new Yaml(new EntityConstructor(world, physics));
    //yaml.setBeanAccess(BeanAccess.FIELD);
    Entity entity = (Entity)yaml.load(serials.get(type));
    entity.addToWorld();
    System.out.println("created " + type.toLowerCase());
    return entity;
  }

  public static String dumpEntity(String path, Entity entity) {
    Yaml yaml = new Yaml(new EntityRepresenter());
    //yaml.setBeanAccess(BeanAccess.FIELD);
    String dump = yaml.dump(entity);
    try {
      URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
      File file = new File(url.getPath());
      path = file.getParent().replace('\\', '/') + "/" + path;
      file = new File(path);
      PrintWriter writer = new PrintWriter(file);

      String[] lines = dump.split("\n");
      for(int i = 0; i < lines.length; i++)
        writer.println(lines[i]);
      writer.close();
    }
    catch (FileNotFoundException e) { e.printStackTrace(); }
    return dump;
  }

  public static String dumpEntity(Entity entity) {
    String filename = entity.getWorld().getManager(TypeManager.class).getType(entity);
    return dumpEntity("/yaml/" + filename + ".yml", entity);
    /*
       try
       {
       URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
       File file = new File(url.getPath());
       String path = file.getParent().replace('\\', '/') + "/yaml/" + filename + ".yml";
       file = new File(path);
       PrintWriter writer = new PrintWriter(file);

       String[] lines = dump.split("\n");
       for(int i = 0; i < lines.length; i++)
       writer.println(lines[i]);
       writer.close();
       }
       catch (FileNotFoundException e) { e.printStackTrace(); }
       return dump;
       */
  }

  public static String dumpEntityAs(String name, Entity entity) {
    return dumpEntity("/yaml/" + name + ".yml", entity);
  }

  public static void resetEntities(World world, PhysicsSystem physics) {
    loadPrototypes();
    Yaml yaml = new Yaml(new EntityConstructor(world, physics));
    ComponentMapper<Transform> tm = ComponentMapper.getFor(Transform.class, world);
    ComponentMapper<Agent> pm = ComponentMapper.getFor(Agent.class, world);

    for(Map.Entry<String, String> entry : serials.entrySet()) {
      //Iterator<Integer> iterator = world.getManager(TypeManager.class).getEntities(entry.getKey()).iterator();
      ImmutableBag<Entity> bag = world.getManager(GroupManager.class).getEntities(entry.getKey());
      ArrayList<Entity> entities = new ArrayList<Entity>();
      for(int i = 0; i < bag.size(); i++)
        entities.add(bag.get(i));

      for(Entity original : entities) {
        Entity replace = (Entity)yaml.load(entry.getValue());
        if(tm.has(original)) {
          Transform t = tm.get(original);
          tm.get(replace).setPosition(t.getPosition());
          tm.get(replace).setRotation(t.getRotation());
        }
        if(pm.has(original)) {
          org.jbox2d.common.Transform t = pm.get(original).getBody().getTransform();
          pm.get(replace).getBody().setTransform(t.p, t.q.getAngle());
          pm.get(replace).getBody().setLinearVelocity(pm.get(original).getBody().getLinearVelocity());
          pm.get(replace).getBody().setAngularVelocity(pm.get(original).getBody().getAngularVelocity());
        }

        replace.addToWorld();
        if(!original.isEnabled())
          world.disable(replace);
        original.deleteFromWorld();
      }
    }
  }

  public static void resetComponent(Entity entity, Class<? extends Component> component) {
    String type = entity.getWorld().getManager(TypeManager.class).getType(entity);
    Component c = component.cast(prototypes.get(type).getComponent(component));
    if(c instanceof Copyable)
      entity.addComponent(component.cast(((Copyable) c).copy()));
  }

  public static Entity createPlayer(World entityWorld, PhysicsSystem physics, Vec2 position) {
    Entity player = entityWorld.createEntity();
    CircleShape hitbox = new CircleShape();
    hitbox.setRadius(.38f);
    FixtureDef fixtureDef = new FixtureDef();
    fixtureDef.restitution = 1;
    fixtureDef.shape = hitbox;
    fixtureDef.density = 1;
    BodyDef bodyDef = new BodyDef();
    bodyDef.position = position;
    bodyDef.type = BodyType.DYNAMIC;
    bodyDef.linearDamping = 3f;
    bodyDef.angularDamping = 10f;
    Body body = physics.getPhysicsWorld().createBody(bodyDef);
    body.createFixture(fixtureDef);
    body.setUserData(player);
    player.addComponent(new Transform(position, 0));
    player.addComponent(new Spatial(hitbox));
    player.addComponent(new Agent(body, new Vec2(0, -0.11f), 0.04f));
    player.addComponent(new Sprite("/images/gs_player.png", 4, new Vec2(0, -0.12f)));
    player.addComponent(new Health(6, 1000));
    entityWorld.getManager(TagManager.class).register("PLAYER", player);
    entityWorld.getManager(GroupManager.class).add(player, "PLAYER");
    player.addToWorld();
    return player;
  }

  public static Entity createArena(World entityWorld, int ngon, float radius) {
    Entity arena = entityWorld.createEntity();
    PolygonShape shape = new PolygonShape();
    Vec2[] vert = new Vec2[ngon];
    double angle = Math.PI - (((ngon - 2) * Math.PI) / ngon);
    for(int i = 0; i < ngon; i++) {
      vert[i] = new Vec2((float)Math.cos(angle*i+Math.PI/2), (float)Math.sin(angle*i+Math.PI/2)).mul(radius);
    }
    shape.set(vert, ngon);
    arena.addComponent(new Spatial(shape, false));
    entityWorld.getManager(TagManager.class).register("ARENA", arena);
    entityWorld.getManager(GroupManager.class).add(arena, "ARENA");
    arena.addToWorld();
    return arena;
  }

  public static Entity createWall(World entityWorld, PhysicsSystem physics, Vec2 v1, Vec2 v2) {
    Entity wall = entityWorld.createEntity();
    EdgeShape hitbox = new EdgeShape();
    hitbox.set(v1, v2);
    BodyDef bodyDef = new BodyDef();
    bodyDef.type = BodyType.STATIC;
    Body body = physics.getPhysicsWorld().createBody(bodyDef);
    body.createFixture(hitbox, 1);
    body.setUserData(wall);
    wall.addComponent(new Transform(v1, 0));
    //wall.addComponent(new Spatial(hitbox));
    wall.addComponent(new Agent(body, new Vec2(0, 0), 0));
    entityWorld.getManager(GroupManager.class).add(wall, "WALL");
    entityWorld.getManager(TypeManager.class).setType(wall, "WALL");
    wall.addToWorld();
    return wall;
  }

  // main method for quickly modifying entities and their yaml representations
  public static void main(String[] args) {
    World world = new World();
    world.setManager(new GroupManager());
    world.setManager(new TagManager());
    world.setManager(new TypeManager());
    world.setManager(new EventManager());
    PhysicsSystem physics = new PhysicsSystem();
    world.setSystem(physics);
    world.initialize();
    loadPrototypes();

    Entity entity = loadEntity("SHARD", world, physics);
    Steering steering = new Steering();
    Boid boid = new Boid("SEEK", 1, 3, 1);
    steering.addTarget("PLAYER", boid);
    entity.addComponent(steering);

    dumpEntity(entity);
  }
}
