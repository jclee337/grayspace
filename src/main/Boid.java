package main;

/**
 *  This class represents a particular steering behavior
 *  of some entity
 *  Called a boid for lack of  better name
 *  See: 
 *    main.components.Steering
 *    main.systems.SteeringSystem
 */
public class Boid {
  public String behavior;
  public double strength;
  public double distance;
  public int spread;

  public Boid() {

  }

  public Boid(String behavior, double strength, double distance, int spread) {
    this.behavior = behavior;
    this.strength = strength;
    this.distance = distance;
    this.spread = spread;
  }

  public String getBehavior() {
    return behavior;
  }

  public void setBehavior(String behavior) {
    this.behavior = behavior;
  }

  public double getStrength() {
    return strength;
  }

  public void setStrength(double strength) {
    this.strength = strength;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  public int getSpread() {
    return spread;
  }

  public void setSpread(int spread) {
    this.spread = spread;
  }

  @Override
  public boolean equals(Object boid) {
    if(boid instanceof Boid)
      return behavior.equals(((Boid)boid).behavior);
    return false;
  }
}
