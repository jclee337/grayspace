package main;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import main.components.*;
import main.events.EntityMovedEvent;
import main.systems.*;
import main.systems.render.RenderSystem;
import main.systems.render.SpatialRenderSystem;
import main.systems.render.SpriteRenderSystem;
import org.jbox2d.callbacks.ContactFilter;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Fixture;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.glBlendEquation;

public class Main {
  private int width = 1280;
  private int height = 720;
  //private int width = 960;
  //private int height = 540;
  //private int width = 1600;
  //private int height = 900;
  private long deltaTime; // the amount of time that has passed since the last tick
  private long fps;       // however many ticks (frames) happen per second
  private boolean oneShot = true;
  private com.artemis.World world;

  public static void main(String[] args) {
    new Main();
  }

  public Main() {
    PixelFormat pixelFormat = new PixelFormat();
    ContextAttribs contextAttributes = new ContextAttribs(3, 2);
    contextAttributes.withForwardCompatible(true);
    contextAttributes.withProfileCore(true);

    try {
      Display.setDisplayMode(new DisplayMode(width, height));
      Display.setTitle("Gray Space");
      //ByteBuffer[] list = new ByteBuffer[2];
      //try
      //{
      //list[0] = loadIcon("images/icon16.png", 16, 16);
      //list[1] = loadIcon("images/icon32.png", 32, 32);
      //}a
      //catch (IOException e) { e.printStackTrace(); }
      //Display.setIcon(list);
      Display.setInitialBackground(.0f, .0f, .0f);
      //Display.create(pixelFormat, contextAttributes);
      Display.create();

      // Controller controller = ControllerEnvironment.getDefaultEnvironment.getControllers();

      // enable textures since we're going to use these for our sprites
      glEnable(GL_TEXTURE_2D);

      glShadeModel(GL11.GL_SMOOTH);

      //glEnable(GL_DEPTH_TEST);
      //glDepthFunc(GL_LESS);

      // enable transparency
      glEnable(GL11.GL_BLEND);
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
      glBlendEquation(GL14.GL_FUNC_ADD);

      // disable the OpenGL depth test since we're rendering 2D graphics
      glDisable(GL_DEPTH_TEST);

      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();

      glOrtho(0, width, height, 0, 10, -10);
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glViewport(0, 0, width, height);
    }
    catch (LWJGLException e) { e.printStackTrace(); }

    world = new com.artemis.World();
    world.setManager(new GroupManager());
    world.setManager(new TagManager());
    world.setManager(new TypeManager());
    world.setManager(new EventManager());
    world.setManager(new PoolManager());

    PhysicsSystem physics = new PhysicsSystem();
    CollisionSystem collision = new CollisionSystem();
    physics.getPhysicsWorld().setContactListener(collision);
    physics.getPhysicsWorld().setContactFilter(new ContactFilter() {
      @Override
      public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {
        String a = world.getManager(TypeManager.class).getType((Entity)fixtureA.getBody().getUserData());
        String b = world.getManager(TypeManager.class).getType((Entity)fixtureB.getBody().getUserData());
        if((a.equals("SWARM") && b.equals("WALL")) || (a.equals("WALL") && b.equals("SWARM")))
          return true;
        else if(a.equals("SWARM") || b.equals("SWARM"))
          return false;
        else if((a.equals("SHARD") && b.equals("PLAYER")) || (a.equals("PLAYER") && b.equals("SHARD")))
          return true;
        else if((a.equals("SHARD") && b.equals("WALL")) || (a.equals("WALL") && b.equals("SHARD")))
          return true;
        else if((a.equals("SHARD") && b.equals("CLEANER")) || (a.equals("CLEANER") && b.equals("SHARD")))
          return true;
        else if(a.equals("SHARD") || b.equals("SHARD"))
          return false;
        return true;
      }
    });
    RenderSystem render = new RenderSystem();
    EntityFactory.loadPrototypes();
    //world.setSystem(new InputSystem());
    world.setSystem(collision);
    world.setSystem(physics);
    world.setSystem(new ArenaSystem());
    world.setSystem(new CameraSystem());
    world.setSystem(new PlayerTrailSystem());
    world.setSystem(new PlayerControlSystem());
    world.setSystem(new PlayerWeaponSystem());
    world.setSystem(new SteeringSystem());
    world.setSystem(new EnemySpawnSystem());
    world.setSystem(new HealthSystem());
    world.setSystem(new ScoreSystem());
    world.setSystem(render);
    render.setRenderer(new SpriteRenderSystem());
    render.setRenderer(new SpatialRenderSystem());
    world.setSystem(new HudRenderSystem());
    world.setSystem(new DeveloperSystem());

    world.initialize();

    Entity arena = EntityFactory.loadEntity("ARENA", world, physics);
    Entity camera = EntityFactory.loadEntity("CAMERA", world, physics);
    Entity player = EntityFactory.loadEntity("PLAYER", world, physics);
    Entity guns = EntityFactory.loadEntity("PLAYER_GUNS", world, physics);
    EntityFactory.loadEntity("SWARM", world, physics);
    EntityFactory.loadEntity("SWARM", world, physics);
    EntityFactory.loadEntity("SWARM", world, physics);
    for(int i = 0; i < 20; i++) {
      Entity pointer = EntityFactory.loadEntity("POINTER", world, physics);
      pointer.disable();
    }
    for(int i = 0; i < 45; i++) {
      Entity blocker = EntityFactory.loadEntity("BLOCKER", world, physics);
      world.getManager(EventManager.class).broadcast(new EntityMovedEvent(blocker, new Vec2((i * 0.3f)+1, 0)));
    }

    PolygonShape shape = (PolygonShape)arena.getComponent(Spatial.class).getShape();
    for(int i = 0; i < shape.getVertexCount(); i++) {
      int j = (i + 1 == shape.getVertexCount()) ? 0 : i + 1;
      EntityFactory.createWall(world, world.getSystem(PhysicsSystem.class), shape.getVertex(i), shape.getVertex(j));
    }

    run();
  }

  public void run() {
    double start, end, totalTime = 0;
    deltaTime = 1;
    int ticks = 0;
    while(!Display.isCloseRequested()) {
      start = (Sys.getTime() * 1e9) / Sys.getTimerResolution();
      update();
      end = (Sys.getTime() * 1e9) / Sys.getTimerResolution();
      ticks++;
      // 1e6 nanoseconds in 1 millisecond
      deltaTime = (long)(end - start);
      totalTime += deltaTime;
      if(totalTime >= 1e9) { // 1e9 nanoseconds in 1 second
        fps = ticks;
        ticks = 0;
        totalTime -= 1e9;
      }
    }
    Display.destroy();
  }

  public void update() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    Display.sync(60);
    //world.setDelta(deltaTime / 1e6f);
    world.setDelta(1000 / 60);
    world.process();

    world.getSystem(HudRenderSystem.class).setFPS((int)fps);

    Display.update();
  }
}
