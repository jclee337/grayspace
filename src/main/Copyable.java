package main;

public interface Copyable {
  public abstract Object copy();
}
