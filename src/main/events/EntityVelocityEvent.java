package main.events;

import com.artemis.Entity;
import org.jbox2d.common.Vec2;

public class EntityVelocityEvent extends Event
{
    public Entity entity;
    public Vec2 velocity;

    public EntityVelocityEvent(Entity entity, Vec2 velocity)
    {
        this.entity = entity;
        this.velocity = velocity;
    }
}
