package main.events;

import main.events.Event;

public class InputEvent extends Event
{
    public int key;
    public boolean pressed;

    public InputEvent(int key, boolean pressed)
    {
        this.key = key;
        this.pressed = pressed;
    }
}
