package main.events;

import com.artemis.Entity;
import org.jbox2d.common.Vec2;

public class EntityMovedEvent extends Event
{
    public Entity entity;
    public Vec2 position;

    public EntityMovedEvent(Entity entity, Vec2 position)
    {
        this.entity = entity;
        this.position = position;
    }
}
