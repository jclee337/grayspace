package main.events;

import com.artemis.Entity;

public class EntityDiedEvent extends Event
{
    public Entity entity;

    public EntityDiedEvent(Entity entity)
    {
        this.entity = entity;
    }
}
