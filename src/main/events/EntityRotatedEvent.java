package main.events;

import com.artemis.Entity;

public class EntityRotatedEvent extends Event
{
    public Entity entity;
    public float rotation;

    public EntityRotatedEvent(Entity entity, float rotation)
    {
        this.entity = entity;
        this.rotation = rotation;
    }
}
