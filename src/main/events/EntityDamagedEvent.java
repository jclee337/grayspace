package main.events;

import com.artemis.Entity;

public class EntityDamagedEvent extends Event
{
    public Entity entity;

    public EntityDamagedEvent(Entity entity)
    {
        this.entity = entity;
    }
}
