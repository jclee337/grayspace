package main.serialization;

import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;
import org.yaml.snakeyaml.representer.Representer;

import java.util.HashMap;
import java.util.Map;

public class BodyRepresenter extends Representer {
  public BodyRepresenter() {
    this.representers.put(Body.class, new RepresentBody());
    this.representers.put(Fixture.class, new RepresentFixture());
  }

  public class RepresentBody implements Represent {
    @Override
    public Node representData(Object o) {
      Body body = (Body)o;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("m_type", body.m_type);
      map.put("m_flags", body.m_flags);
      map.put("m_islandIndex", body.m_islandIndex);
      map.put("m_xf", body.m_xf);
      map.put("m_linearVelocity", body.m_linearVelocity);
      map.put("m_angularVelocity", body.m_angularVelocity);
      map.put("m_force", body.m_force);
      map.put("m_torque", body.m_torque);
      map.put("m_linearDamping", body.m_linearDamping);
      map.put("m_angularDamping", body.m_angularDamping);
      map.put("m_gravityScale", body.m_gravityScale);
      map.put("m_sleepTime", body.m_sleepTime);
      map.put("m_fixtureList", body.m_fixtureList);
      return representMapping(new Tag("!body"), map, false);
    }
  }

  public class RepresentFixture implements Represent {
    @Override
    public Node representData(Object o) {
      Fixture fixture = (Fixture)o;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("m_density", fixture.m_density);
      map.put("m_shape", fixture.m_shape);
      map.put("m_friction", fixture.m_friction);
      map.put("m_restitution", fixture.m_restitution);
      map.put("m_isSensor", fixture.m_isSensor);
      return representMapping(new Tag("!fixture"), map, false);
    }
  }
}
