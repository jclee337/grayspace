package main.serialization;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import main.TypeManager;
import main.components.Agent;
import main.systems.PhysicsSystem;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.constructor.Construct;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;

import java.util.List;
import java.util.Map;

public class EntityConstructor extends Constructor {
  World world;
  PhysicsSystem physics;

  public EntityConstructor(World world, PhysicsSystem physics) {
    this.world = world;
    this.physics = physics;
    this.yamlConstructors.put(new Tag("!entity"), new ConstructEntity());
    this.yamlConstructors.put(new Tag("!body"), new ConstructBody());
    this.yamlConstructors.put(new Tag("!fixture"), new ConstructFixture());
    this.addTypeDescription(new TypeDescription(main.Boid.class, "!boid"));
  }

  private class ConstructEntity implements Construct {
    @Override
    public Object construct(Node node) {
      Entity entity = world.createEntity();
      Map<Object, Object> map = constructMapping((MappingNode)node);

      if(!(Boolean)map.get("enabled"))
        entity.disable();

      List<Component> components = (List<Component>)map.get("components");
      for(Component c : components) {
        if (c instanceof Agent)
          ((Agent) c).getBody().setUserData(entity);
        entity.addComponent(c);
      }

      String tag = (String)map.get("tag");

      if(tag != null) {
        world.getManager(TagManager.class).unregister(tag);
        world.getManager(TagManager.class).register(tag, entity);
      }

      List<String> groups = (List<String>)map.get("groups");
      for(String s : groups) {
        world.getManager(GroupManager.class).add(entity, s);
      }

      String type = (String)map.get("type");
      world.getManager(TypeManager.class).setType(entity, type);

      return entity;
    }

    @Override
    public void construct2ndStep(Node node, Object o) {

    }
  }

  private class ConstructBody implements Construct {
    @Override
    public Object construct(Node node) {
      BodyDef bodyDef = new BodyDef();
      Map<Object, Object> map = constructMapping((MappingNode)node);
      bodyDef.type = (BodyType)map.get("m_type");
      bodyDef.linearVelocity = (Vec2)map.get("m_linearVelocity");
      bodyDef.angularVelocity = ((Double)map.get("m_angularVelocity")).floatValue();
      bodyDef.linearDamping = ((Double)map.get("m_linearDamping")).floatValue();
      bodyDef.angularDamping = ((Double)map.get("m_angularDamping")).floatValue();
      bodyDef.gravityScale = ((Double)map.get("m_gravityScale")).floatValue();

      Body body = physics.getPhysicsWorld().createBody(bodyDef);
      body.m_flags = (Integer)map.get("m_flags");
      body.m_islandIndex = (Integer)map.get("m_islandIndex");
      Transform transform = (Transform)map.get("m_xf");
      body.setTransform(transform.p, transform.q.getAngle());
      body.applyForceToCenter((Vec2)map.get("m_force"));
      body.applyTorque(((Double)map.get("m_torque")).floatValue());
      body.m_sleepTime = ((Double)map.get("m_sleepTime")).floatValue();
      List<FixtureDef> fixtures = (List<FixtureDef>)map.get("m_fixtureList");
      for(FixtureDef fixture : fixtures)
        body.createFixture(fixture);
      return body;
    }

    @Override
    public void construct2ndStep(Node node, Object o) {

    }
  }

  private class ConstructFixture implements Construct {
    @Override
    public Object construct(Node node) {
      FixtureDef fixtureDef = new FixtureDef();
      Map<Object, Object> map = constructMapping((MappingNode)node);
      fixtureDef.density = ((Double)map.get("m_density")).floatValue();
      fixtureDef.shape = (Shape)map.get("m_shape");
      fixtureDef.friction = ((Double)map.get("m_friction")).floatValue();
      fixtureDef.restitution = ((Double)map.get("m_restitution")).floatValue();
      fixtureDef.isSensor = (Boolean)map.get("m_isSensor");
      return fixtureDef;
    }

    @Override
    public void construct2ndStep(Node node, Object o) {

    }
  }
}
