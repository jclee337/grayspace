package main.serialization;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.TeamManager;
import com.artemis.utils.Bag;
import com.artemis.utils.ImmutableBag;
import main.TypeManager;
import main.components.Steering;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Represent;
import org.yaml.snakeyaml.representer.Representer;

import java.util.*;

public class EntityRepresenter extends Representer {
  public EntityRepresenter() {
    this.representers.put(Entity.class, new RepresentEntity());
    this.representers.put(Body.class, new RepresentBody());
    this.representers.put(Fixture.class, new RepresentFixture());
    this.addClassTag(main.Boid.class, new Tag("!boid"));
    this.addClassTag(Body.class, new Tag("!body"));
  }

  private class RepresentEntity implements Represent {
    @Override
    public Node representData(Object o) {
      Entity entity = (Entity)o;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("enabled", entity.isEnabled());

      Bag<Component> bag = new Bag<Component>();
      bag = entity.getComponents(bag);
      List<Component> components = new ArrayList<Component>();
      for(int i = 0; i < bag.size(); i++)
        components.add(bag.get(i));
      map.put("components", components);

      Collection<String> tags = entity.getWorld().getManager(TagManager.class).getRegisteredTags();
      for(String s : tags) {
        if(entity.getWorld().getManager(TagManager.class).getEntity(s).equals(entity)) {
          map.put("tag", s);
          break;
        }
      }
      ImmutableBag<String> groupBag = entity.getWorld().getManager(GroupManager.class).getGroups(entity);
      if(groupBag != null) {
        List<String> groups = new ArrayList<String>();
        for(int i = 0; i < groupBag.size(); i++)
          groups.add(groupBag.get(i));
        map.put("groups", groups);
      }
      String type = entity.getWorld().getManager(TypeManager.class).getType(entity);
      map.put("type", type);

      return representMapping(new Tag("!entity"), map, false);
    }
  }

  public class RepresentBody implements Represent {
    @Override
    public Node representData(Object o) {
      Body body = (Body)o;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("m_type", body.m_type);
      map.put("m_flags", body.m_flags);
      map.put("m_islandIndex", body.m_islandIndex);
      map.put("m_xf", body.m_xf);
      map.put("m_linearVelocity", body.m_linearVelocity);
      map.put("m_angularVelocity", body.m_angularVelocity);
      map.put("m_force", body.m_force);
      map.put("m_torque", body.m_torque);
      map.put("m_linearDamping", body.m_linearDamping);
      map.put("m_angularDamping", body.m_angularDamping);
      map.put("m_gravityScale", body.m_gravityScale);
      map.put("m_sleepTime", body.m_sleepTime);
      List<Fixture> fixtures = new ArrayList<Fixture>();
      Fixture fixture = body.m_fixtureList;
      while(fixture != null) {
        fixtures.add(fixture);
        fixture = fixture.getNext();
      }
      map.put("m_fixtureList", fixtures);
      return representMapping(new Tag("!body"), map, false);
    }
  }

  public class RepresentFixture implements Represent {
    @Override
    public Node representData(Object o) {
      Fixture fixture = (Fixture)o;
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("m_density", fixture.m_density);
      map.put("m_shape", fixture.m_shape);
      map.put("m_friction", fixture.m_friction);
      map.put("m_restitution", fixture.m_restitution);
      map.put("m_isSensor", fixture.m_isSensor);
      return representMapping(new Tag("!fixture"), map, false);
    }
  }
}
