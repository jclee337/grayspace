package main.serialization;

import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.*;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Construct;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;

public class BodyConstructor extends Constructor {
  World world;

  public BodyConstructor(World world) {
    this.world = world;
    this.yamlConstructors.put(new Tag("!!org.jbox2d.dynamics.Body"), new ConstructBody());
  }

  private class ConstructBody implements Construct {
    @Override
    public Object construct(Node node) {
      Yaml yaml = new Yaml();
      // this might get a little confusing
      String value = (String)constructScalar((ScalarNode)node);
      String[] split = value.split("\r\n");
      //for(String s : split) {
      //System.out.println(s);
      //}

      BodyDef bodyDef = new BodyDef();
      String str = split[0].split("m_type: ")[1];
      if(str.equals(BodyType.DYNAMIC.name()))
        bodyDef.type = BodyType.DYNAMIC;
      else if(str.equals(BodyType.KINEMATIC.name()))
        bodyDef.type = BodyType.KINEMATIC;
      else if(str.equals(BodyType.STATIC.name()))
        bodyDef.type = BodyType.STATIC;
      str = split[4].split("m_linearVelocity: ")[1];
      bodyDef.linearVelocity = (Vec2)yaml.load(str);
      str = split[5].split("m_angularVelocity: ")[1];
      bodyDef.angularVelocity = Float.parseFloat(str);
      str = split[8].split("m_linearDamping: ")[1];
      bodyDef.linearDamping = Float.parseFloat(str);
      str = split[9].split("m_angularDamping: ")[1];
      bodyDef.angularDamping = Float.parseFloat(str);
      str = split[10].split("m_gravityScale: ")[1];
      bodyDef.gravityScale = Float.parseFloat(str);


      Body body = world.createBody(bodyDef);
      str = split[1].split("m_flags: ")[1];
      //System.out.println(str);
      body.m_flags = Integer.parseInt(str);
      str = split[2].split("m_islandIndex: ")[1];
      body.m_islandIndex = Integer.parseInt(str);
      str = split[3].split("m_xf: ")[1];
      Transform transform = (Transform)yaml.load(str);
      body.setTransform(transform.p, transform.q.getAngle());
      str = split[6].split("m_force: ")[1];
      body.applyForceToCenter((Vec2)yaml.load(str));
      str = split[7].split("m_torque: ")[1];
      body.applyTorque(Float.parseFloat(str));
      str = split[11].split("m_sleepTime: ")[1];
      body.m_sleepTime = Float.parseFloat(str);


      str = value.split("m_fixtureList: ")[1];
      FixtureDef fixtureDef = (FixtureDef)yaml.load(str);
      body.createFixture(fixtureDef);
      return body;
    }

    @Override
    public void construct2ndStep(Node node, Object o) {

    }
  }
}
