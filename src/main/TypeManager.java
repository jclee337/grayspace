package main;

import com.artemis.Entity;
import com.artemis.Manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TypeManager extends Manager {
  private HashMap<String, List<Integer>> entitiesByType;
  private HashMap<Integer, String> typeByEntity;

  public TypeManager() {
    entitiesByType = new HashMap<String, List<Integer>>();
    typeByEntity = new HashMap<Integer, String>();
  }

  @Override
  protected void initialize() {

  }

  public String getType(Entity entity) {
    return typeByEntity.get(entity.getId());
  }

  public List<Integer> getEntities(String type) {
    return entitiesByType.get(type);
  }

  public void setType(Entity entity, String type) {
    typeByEntity.put(entity.getId(), type);

    List<Integer> entities = entitiesByType.get(type);
    if(entities == null)
      entitiesByType.put(type, new ArrayList<Integer>(Arrays.asList(entity.getId())));
    else
      entities.add(entity.getId());
  }

  @Override
  public void deleted(Entity entity) {
    String type = typeByEntity.get(entity.getId());
    if(type != null) {
      typeByEntity.remove(entity.getId());
      entitiesByType.get(type).remove((Object)entity.getId());
    }
  }
}
