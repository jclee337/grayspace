package main.components;

import com.artemis.Component;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

public class Agent extends Component {
  private Body body;
  private Vec2 thrust;
  private double spin;
  private boolean impulsed;

  public Agent() {
    this.body = null;
    this.thrust = new Vec2(0, 0);
    this.spin = 0;
    this.impulsed = false;
  }

  public Agent(Body body, Vec2 thrust, double spin) {
    this.body = body;
    this.thrust = thrust;
    this.spin = spin;
    this.impulsed = false;
  }

  public Body getBody() {
    return body;
  }

  public void setBody(Body body) {
    this.body = body;
  }

  public Vec2 getThrust() {
    return thrust;
  }

  public void setThrust(Vec2 thrust) {
    this.thrust = thrust;
  }

  public double getSpin() {
    return spin;
  }

  public void setSpin(double spin) {
    this.spin = spin;
  }

  public boolean isImpulsed() {
    return impulsed;
  }

  public void setImpulsed(boolean impulsed) {
    this.impulsed = impulsed;
  }

  public float getMaxVelocity() {
    return thrust.length() * body.m_invMass / body.getLinearDamping();
  }
}
