package main.components;

import com.artemis.Component;

public class Camera extends Component {
  private double zoom;

  public Camera() {}

  public Camera(double zoom) {
    this.zoom = zoom;
  }

  public double getZoom() {
    return zoom;
  }

  public void setZoom(double zoom) {
    this.zoom = zoom;
  }
}
