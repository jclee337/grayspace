package main.components;

import com.artemis.Component;

public class Weapon extends Component {
  private double shotAt, delay;

  public Weapon() {}

  public Weapon(double delay) {
    this.shotAt = 0;
    this.delay = delay;
  }

  public double getShotAt() {
    return shotAt;
  }

  public void setShotAt(double shotAt) {
    this.shotAt = shotAt;
  }

  public double getDelay() {
    return delay;
  }

  public void setDelay(double delay) {
    this.delay = delay;
  }
}
