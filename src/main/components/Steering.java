package main.components;

import com.artemis.Component;
import main.Boid;
import main.Copyable;

import java.util.*;

/**
 *  This component stores data about an entity's steering behaviors (Boids)
 *  and also the result of processing those behaviors after each game step
 *  See: 
 *    main.Boid
 *    main.systems.SteeringManager
 */
public class Steering extends Component implements Copyable {
  public Double danger[];
  public Double interest[];
  public Double align[];
  public Double disalign[];
  public LinkedHashMap<String, ArrayList<Boid>> targets = new LinkedHashMap<String, ArrayList<Boid>>();

  public Steering() {
    danger = new Double[8];
    interest = new Double[8];
    disalign = new Double[2];
    align = new Double[2];
    clearMaps();
  }

  public Steering(LinkedHashMap<String, ArrayList<Boid>> targets) {
    danger = new Double[8];
    interest = new Double[8];
    disalign = new Double[2];
    align = new Double[2];
    clearMaps();
    this.targets = targets;
  }

  private int wrapIndex(int index) {
    while(index < 0)
      index += danger.length;
    while(index >= danger.length)
      index -= danger.length;
    return index;
  }

  /*
     takes the direction and magnitude of something interesting and
     splits that interest across multiple slots on the map
     */
  public void splitInterest(double angle, double interest, int spread) {
    double index = angle / (2*Math.PI / danger.length);
    double gradient = index - Math.floor(index);
    int up = (int)Math.ceil(index);
    int down = (int)Math.floor(index);
    for(; spread > 0; spread--) {
      placeInterest(up, interest * gradient);
      placeInterest(down, interest * (1 - gradient));
      up = wrapIndex(up + 1);
      down = wrapIndex(down - 1);
      interest *= .5;
    }
  }

  /*
     takes the direction and magnitude of something dangerous and
     splits that danger across multiple slots on the map
     */
  public void splitDanger(double angle, double danger, int spread) {
    double index = angle / (2*Math.PI / interest.length);
    double gradient = index - Math.floor(index);
    int up = (int)Math.ceil(index);
    int down = (int)Math.floor(index);
    for(; spread > 0; spread--) {
      placeDanger(up, danger * gradient);
      placeDanger(down, danger * (1 - gradient));
      up = wrapIndex(up + 1);
      down = wrapIndex(down - 1);
      danger *= .5;
    }
  }

  // returns true if slot is overwritten
  public boolean placeInterest(int index, double interest) {
    if(index == this.interest.length)
      index = 0;
    if(interest > this.interest[index]) {
      this.interest[index] = interest;
      return true;
    }
    return false;
  }

  public boolean placeDanger(int index, double danger) {
    if(index == this.danger.length)
      index = 0;
    if(danger > this.danger[index]) {
      this.danger[index] = danger;
      return true;
    }
    return false;
  }

  public boolean placeAlign(double align) {
    if(align > 0 && align > this.align[1]) {
      this.align[1] = align;
      return true;
    } else if(align < 0 && Math.abs(align) > this.align[0]) {
      this.align[0] = Math.abs(align);
      return true;
    }
    return false;
  }

  public void addTarget(String target, Boid boid) {
    if(targets.containsKey(target)) {
      ArrayList<Boid> boids = targets.get(target);
      boids.remove(boid);
      boids.add(boid);
    }
    else
      targets.put(target, new ArrayList<Boid>(Arrays.asList(boid)));
  }

  public void clearMaps() {
    for(int i = 0; i < danger.length; i++)
      danger[i] = 0.0;
    for(int i = 0; i < interest.length; i++)
      interest[i] = 0.0;
    disalign[0] = 0.0;
    disalign[1] = 0.0;
    align[0] = 0.0;
    align[1] = 0.0;
  }

  @Override
  public Object copy() {
    return new Steering(new LinkedHashMap<String, ArrayList<Boid>>(targets));
  }
}
