package main.components;

import com.artemis.Component;
import org.jbox2d.collision.shapes.Shape;
import org.lwjgl.util.Color;

public class Spatial extends Component {
  private Shape shape;
  private boolean visible, filled;
  private int layer;
  private Color color;

  public Spatial() {}

  public Spatial(Shape shape) {
    this.shape = shape;
    this.visible = true;
    this.filled = false;
    this.layer = 0;
    this.color = new Color(255, 255, 255);
  }

  public Spatial(Shape shape, boolean visible) {
    this.shape = shape;
    this.visible = visible;
    this.filled = false;
  }

  public Shape getShape() {
    return shape;
  }

  public void setShape(Shape shape) {
    this.shape = shape;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }

  public boolean isFilled() {
    return filled;
  }

  public void setFilled(boolean filled) {
    this.filled = filled;
  }

  public int getLayer() {
    return layer;
  }

  public void setLayer(int layer) {
    this.layer = layer;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }
}
