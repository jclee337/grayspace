package main.components;

import com.artemis.Component;
import org.jbox2d.common.Vec2;

public class Transform extends Component {
  private Vec2 position;
  private double rotation;

  public Transform() {
    position = new Vec2(0, 0);
    rotation = 0;
  }

  public Transform(Vec2 position, double rotation) {
    this.position = position;
    this.rotation = rotation;
  }

  public Vec2 getPosition() {
    return position;
  }

  public void setPosition(Vec2 position) {
    this.position = position;
  }

  public double getRotation() {
    return rotation;
  }

  public void setRotation(double rotation) {
    this.rotation = rotation;
  }
}
