package main.components;

import com.artemis.Component;
import main.Copyable;

public class Health extends Component implements Copyable {
  private double max, health;
  private long invulnDuration, hitAt;
  private boolean invincible;

  public Health() {}

  public Health(double health, long invulnDuration) {
    this.health = health;
    this.max = health;
    this.invulnDuration = invulnDuration;
  }

  public Health(double health, long invulnDuration, boolean invincible) {
    this.health = health;
    this.max = health;
    this.invulnDuration = invulnDuration;
    this.invincible = invincible;
  }

  public double getHealth() {
    return health;
  }

  public void setHealth(double health) {
    this.health = health;
  }

  public void mutHealth(double health) {
    this.health += health;
  }

  public double getMax() {
    return max;
  }

  public void setMax(double max) {
    this.max = max;
  }

  public long getInvulnDuration() {
    return invulnDuration;
  }

  public void setInvulnDuration(long invulnDuration) {
    this.invulnDuration = invulnDuration;
  }

  public long getHitAt() {
    return hitAt;
  }

  public void setHitAt(long hitAt) {
    this.hitAt = hitAt;
  }

  public boolean isInvincible() {
    return invincible;
  }

  public void setInvincible(boolean invincible) {
    this.invincible = invincible;
  }

  @Override
  public Object copy() {
    return new Health(health, invulnDuration, invincible);
  }
}
