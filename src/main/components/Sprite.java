package main.components;

import com.artemis.Component;
import org.jbox2d.common.Vec2;

public class Sprite extends Component {
  private String path;
  private double z, scale, parallax;
  private Vec2 offset;
  private boolean visible;

  public Sprite() {}

  public Sprite(String path, double z) {
    this.path = path;
    this.z = z;
    this.scale = 1;
    this.parallax = 1;
    this.offset = new Vec2(0, 0);
    this.visible = true;
  }

  public Sprite(String path, double z, Vec2 offset) {
    this.path = path;
    this.z = z;
    this.scale = 1;
    this.parallax = 1;
    this.offset = offset;
    this.visible = true;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String texture) {
    this.path = texture;
  }

  public double getZ() {
    return z;
  }

  public void setZ(double z) {
    this.z = z;
  }

  public double getScale() {
    return scale;
  }

  public void setScale(double scale) {
    this.scale = scale;
  }

  public double getParallax() {
    return parallax;
  }

  public void setParallax(double parallax) {
    this.parallax = parallax;
  }

  public Vec2 getOffset() {
    return offset;
  }

  public void setOffset(Vec2 offset) {
    this.offset = offset;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setVisible(boolean visible) {
    this.visible = visible;
  }
}
