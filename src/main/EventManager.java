package main;

import com.artemis.Manager;
import main.events.Event;
import main.systems.RelayedEntitySystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class EventManager extends Manager {
  private HashMap<Class<? extends Event>, ArrayList<RelayedEntitySystem>> listeners;

  public EventManager() {

  }

  @Override
  public void initialize() {
    listeners = new HashMap<Class<? extends Event>, ArrayList<RelayedEntitySystem>>();
  }

  public void broadcast(Event event) {
    if(listeners.containsKey(event.getClass())) {
      for (RelayedEntitySystem listener : listeners.get(event.getClass())) {
        listener.addEvent(event);
      }
    }
  }

  public void register(RelayedEntitySystem listener) {
    for(Class<? extends Event> event : listener.getSubjects()) {
      if(listeners.containsKey(event))
        listeners.get(event).add(listener);
      else
        listeners.put(event, new ArrayList<RelayedEntitySystem>(Arrays.asList(listener)));
    }
  }

  public void unregister(RelayedEntitySystem listener) {

  }
}
