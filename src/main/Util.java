package main;

import org.jbox2d.common.Vec2;

import java.util.HashMap;

public class Util {
  public static double mod(double a, double n) {
    return (a % n + n) % n;
  }

  public static Vec2 direction(double angle) {
    return new Vec2((float)(Math.cos(angle - (Math.PI / 2))), (float)(Math.sin(angle - (Math.PI / 2))));
  }

  public static double dot(Vec2 v1, Vec2 v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
  }

  public static double random(double lower, double upper) {
    return (Math.random() * (upper - lower)) + lower;
  }

  public static float random(float lower, float upper) {
    return (float)(Math.random() * (upper - lower)) + lower;
  }

  public static long random(long lower, long upper) {
    return (long)(Math.random() * (upper - lower + 1)) + lower;
  }

  public static void main(String[] args) {
    int lower = -16, upper = 0;
    //int freq[] = new int[upper + 1];
    HashMap<Integer, Integer> freq = new HashMap<Integer, Integer>();
    for(int i = 0; i < 100000; i++) {
      int rand = (int) random(lower, upper);
      if(freq.get(rand) == null)
        freq.put(rand, 1);
      else
        freq.put(rand, freq.get(rand) + 1);
    }
    for(int i = lower; i <= upper; i++)
      System.out.println(i + ": " + freq.get(i));
  }
}
