package main.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import main.Boid;
import main.EntityFactory;
import main.TypeManager;
import main.Util;
import main.components.Agent;
import main.components.Spatial;
import main.components.Steering;
import main.components.Transform;
import main.events.EntityDiedEvent;
import main.events.Event;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

public class ScoreSystem extends RelayedEntitySystem {
  private BigInteger score;

  public ScoreSystem() {
    super(Aspect.getEmpty(),
        new ArrayList<Class<? extends Event>>(Arrays.asList(EntityDiedEvent.class)));
    score = new BigInteger("0");
  }

  @Override
  public void process(Entity entity) {

  }

  @Override
  public void processEvent(Event event) {
    if(event instanceof EntityDiedEvent) {
      Entity entity = ((EntityDiedEvent)event).entity;
      String type = world.getManager(TypeManager.class).getType(entity);

      if(type.equals("POINTER")) {
        spawnShards(3, entity.getComponent(Transform.class).getPosition());
        System.out.println("pointer was killed");
      } else if(type.equals("BLOCKER") && Math.random() < 40) {
        spawnShards(1, entity.getComponent(Transform.class).getPosition());
      } else if(type.equals("CLEANER")) {
        spawnShards(5, entity.getComponent(Transform.class).getPosition());
        System.out.println("cleaner was killed");
      } else if(type.equals("SHARD")) {
        score = score.add(new BigInteger("1"));
        //System.out.println("point received");
      }
    }
  }

  private void spawnShards(int num, Vec2 position) {
    for(; num > 0; num--) {
      Entity shard = EntityFactory.getEntity("SHARD", world, world.getSystem(PhysicsSystem.class));
      EntityFactory.resetComponent(shard, Steering.class);
      shard.enable();
      PolygonShape triangle = new PolygonShape();
      triangle.set(new Vec2[] {
        new Vec2(Util.random(-0.15f, 0.15f), -0.3f),
            new Vec2(Util.random(0.1f, 0.3f),   0.3f),
            new Vec2(Util.random(-0.3f, -0.1f),  0.3f)
      }, 3);
      shard.getComponent(Spatial.class).setShape(triangle);
      Body body = shard.getComponent(Agent.class).getBody();
      body.setAngularVelocity(0);
      body.getFixtureList().m_shape = triangle;
      body.setTransform(position, (float)Util.random(0, 2*Math.PI));
      body.applyLinearImpulse(new Vec2(Util.random(-0.001f, 0.001f), Util.random(-0.001f, 0.001f)),
          body.getWorldCenter());
    }
  }

  @Override
  protected boolean checkProcessing() {
    return true;
  }

  public String getScore() {
    return score.toString();
  }
}
