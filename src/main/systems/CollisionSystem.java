package main.systems;

import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import main.Pair;
import main.events.EntityDamagedEvent;
import main.EventManager;
import main.events.EntityDiedEvent;
import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class CollisionSystem extends VoidEntitySystem implements ContactListener {
  HashMap<String, CollisionHandler> handlers;
  LinkedList<Pair<Entity, Entity>> collisions;

  public CollisionSystem() {

  }

  @Override
  protected void initialize() {
    handlers = new HashMap<String, CollisionHandler>();

    addGroup("PLAYER", "ENEMY", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        world.getManager(EventManager.class).broadcast(new EntityDamagedEvent(a));
      }
    });

    addGroup("PLAYER_BULLET", "ENEMY", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        a.disable();
        world.getManager(EventManager.class).broadcast(new EntityDamagedEvent(b));
      }
    });

    addGroup("PLAYER_BULLET", "WALL", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        a.disable();
      }
    });

    addGroup("PLAYER_BULLET", "PLAYER_BULLET", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        a.disable();
        b.disable();
      }
    });

    addGroup("PLAYER_BULLET", "BLOCKER", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        a.disable();
        b.disable();
        world.getManager(EventManager.class).broadcast(new EntityDiedEvent(b));
      }
    });

    addGroup("PLAYER", "SHARD", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        b.disable();
        world.getManager(EventManager.class).broadcast(new EntityDiedEvent(b));
      }
    });

    addGroup("CLEANER", "SHARD", new CollisionHandler() {
      @Override
      public void handleCollision(Entity a, Entity b) {
        b.disable();
      }
    });

    collisions = new LinkedList<Pair<Entity, Entity>>();
  }

  @Override
  protected void processSystem() {
    while(!collisions.isEmpty()) {
      Entity a = collisions.peek().left;
      Entity b = collisions.peek().right;

      ImmutableBag<String> groupsA = world.getManager(GroupManager.class).getGroups(a);
      ImmutableBag<String> groupsB = world.getManager(GroupManager.class).getGroups(b);

      for(int i = 0; i < groupsA.size(); i++) {
        for(int k = 0; k < groupsB.size(); k++) {
          if(handlers.containsKey(groupsA.get(i) + "/" + groupsB.get(k))) {
            CollisionHandler h = handlers.get(groupsA.get(i) + "/" + groupsB.get(k));
            if(h.groupA.equals(groupsA.get(i)))
              h.handleCollision(a, b);
            else
              h.handleCollision(b, a);
          }
        }
      }

      collisions.pop();
    }
  }

  @Override
  public void beginContact(Contact contact) {
    Entity a = (Entity)contact.getFixtureA().getBody().getUserData();
    Entity b = (Entity)contact.getFixtureB().getBody().getUserData();

    if(!a.isEnabled() || !b.isEnabled())
      return;

    collisions.add(new Pair<Entity, Entity>(a, b));
  }

  @Override
  public void endContact(Contact contact) {

  }

  @Override
  public void preSolve(Contact contact, Manifold manifold) {

  }

  @Override
  public void postSolve(Contact contact, ContactImpulse contactImpulse) {

  }

  private void addGroup(String groupA, String groupB, CollisionHandler handler) {
    handler.groupA = groupA;
    handler.groupB = groupB;
    handlers.put(groupA + "/" + groupB, handler);
    handlers.put(groupB + "/" + groupA, handler);
  }

  private abstract class CollisionHandler {
    public String groupA, groupB;
    public abstract void handleCollision(Entity a, Entity b);
  }
}
