package main.systems.render;

import com.artemis.systems.VoidEntitySystem;

import java.util.ArrayList;

public class RenderSystem extends VoidEntitySystem {
  private ArrayList<Renderer> renderers;

  public RenderSystem() {
    renderers = new ArrayList<Renderer>();
  }

  @Override
  protected void processSystem() {
    int size = 0;
    for(Renderer r : renderers)
      size += r.getActives().size();

    for(int i = 0; i < size; i++) {
      int min = Integer.MAX_VALUE, index = 0;
      for(int j = 0; j < renderers.size(); j++) {
        if(renderers.get(j).hasNext() && renderers.get(j).getLayerOfNext() < min) {
          min = renderers.get(j).getLayerOfNext();
          index = j;
        }
      }
      renderers.get(index).processNext();
    }

    for(Renderer r : renderers)
      r.index = 0;
  }

  public void setRenderer(Renderer renderer) {
    renderers.add(renderer);
    world.setSystem(renderer);
  }
}
