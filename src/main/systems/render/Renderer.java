package main.systems.render;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.ImmutableBag;

import java.util.ArrayList;

public abstract class Renderer extends EntitySystem {
  protected ArrayList<Entity> layerOrdered;
  protected int index;

  public Renderer(Aspect aspect) {
    super(aspect);
    layerOrdered = new ArrayList<Entity>();
  }

  public abstract int getLayerOfNext();
  public abstract void process(Entity entity);

  public void processNext() {
    process(layerOrdered.get(index));
    index++;
  }

  public boolean hasNext() {
    return index < layerOrdered.size();
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {

  }
}
