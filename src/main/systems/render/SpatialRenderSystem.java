package main.systems.render;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import main.components.Camera;
import main.components.Spatial;
import main.components.Transform;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glPushMatrix;

public class SpatialRenderSystem extends Renderer {
  @Mapper ComponentMapper<Spatial> sm;
  @Mapper ComponentMapper<Transform> tm;

  public SpatialRenderSystem() {
    super(Aspect.getAspectForAll(Spatial.class, Transform.class));
  }

  @Override
  public void process(Entity entity) {
    Spatial spatial = sm.get(entity);
    Transform transform = tm.get(entity);
    Entity camera = world.getManager(TagManager.class).getEntity("CAMERA");
    Vec2 offset;
    if(!spatial.isVisible())
      return;

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, 0);
    glColor3d(spatial.getColor().getRed() / 255.0,
        spatial.getColor().getGreen() / 255.0,
        spatial.getColor().getBlue() / 255.0);

    double zoom;
    if(camera != null) {
      offset = new Vec2(camera.getComponent(Transform.class).getPosition());
      zoom = camera.getComponent(Camera.class).getZoom();
    } else {
      offset = new Vec2(0, 0);
      zoom = 50;
    }
    glTranslated(-offset.x * zoom, -offset.y * zoom, 0);

    if(spatial.getShape() instanceof CircleShape) {
      int segments = 32;
      double theta = (2*Math.PI) / (double)(segments);
      double tangential = Math.tan(theta);
      double radial = Math.cos(theta);

      double x = spatial.getShape().getRadius() /* * cos(0) */;
      double y = 0 /* spatial.getShape().getRadius() * sin(0) */;

      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glBegin(GL_LINE_STRIP);
      for(int i = 0; i <= segments; i++) {
        glVertex2d((x + transform.getPosition().x) * zoom, (y + transform.getPosition().y) * zoom);

        double tx = -y;
        double ty = x;

        x += tx * tangential;
        y += ty * tangential;

        x *= radial;
        y *= radial;
      }
    } else if(spatial.getShape() instanceof PolygonShape) {
      PolygonShape shape = (PolygonShape)spatial.getShape();
      if(!spatial.isFilled())
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glTranslated(transform.getPosition().x * zoom, transform.getPosition().y * zoom, 0);
      glRotated(Math.toDegrees(transform.getRotation()), 0, 0, 1);
      glBegin(GL_POLYGON);
      for(int i = 0; i < shape.getVertexCount(); i++)
        glVertex2d(shape.getVertex(i).x * zoom, shape.getVertex(i).y * zoom);
    } else if(spatial.getShape() instanceof EdgeShape) {
      EdgeShape shape = (EdgeShape)spatial.getShape();
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glBegin(GL_LINES);
      glVertex2d(shape.m_vertex1.x * zoom, shape.m_vertex1.y * zoom);
      glVertex2d(shape.m_vertex2.x * zoom, shape.m_vertex2.y * zoom);
    }
    glEnd();
    glPopMatrix();
  }

  public int getLayerOfNext() {
    Spatial spatial = sm.get(layerOrdered.get(index));
    return spatial.getLayer();
  }

  @Override
  protected void inserted(Entity e) {
    Spatial sprite = sm.get(e);
    for(int i = 0; i < layerOrdered.size(); i++) {
      Spatial next = sm.get(layerOrdered.get(i));
      if(sprite.getLayer() < next.getLayer()) {
        layerOrdered.add(i, e);
        return;
      }
    }
    layerOrdered.add(e);
  }

  @Override
  protected void removed(Entity e) {
    layerOrdered.remove(e);
  }

  @Override
  protected boolean checkProcessing() {
    return true;
  }
}
