package main.systems.render;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Mapper;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import main.Texture;
import main.TextureLoader;
import main.components.Camera;
import main.components.Sprite;
import main.components.Transform;
import org.jbox2d.common.Vec2;

import java.io.IOException;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glPopMatrix;

public class SpriteRenderSystem extends Renderer {
  @Mapper ComponentMapper<Sprite> sm;
  @Mapper ComponentMapper<Transform> tm;
  TextureLoader loader;

  public SpriteRenderSystem() {
    super(Aspect.getAspectForAll(Sprite.class, Transform.class));
    loader = new TextureLoader();
  }

  public void process(Entity entity) {
    //Entity entity = layerOrdered.get(index);
    Sprite sprite = sm.get(entity);
    Transform transform = tm.get(entity);
    Entity camera = world.getManager(TagManager.class).getEntity("CAMERA");
    Vec2 offset;
    if(!sprite.isVisible())
      return;
    try {
      Texture texture = loader.getTexture(sprite.getPath());
      //System.out.println(sprite.getURL());
      double width = texture.getWidth();
      double height = texture.getHeight();
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glPushMatrix();
      glColor3d(1, 1, 1);
      //offset(pos, angle, z, scale);
      double zoom;
      if(camera != null) {
        offset = new Vec2(camera.getComponent(Transform.class).getPosition());
        zoom = camera.getComponent(Camera.class).getZoom();
      } else {
        offset = new Vec2(0, 0);
        zoom = 50;
      }
      glTranslated((transform.getPosition().x * zoom) - (offset.x * zoom),
          (transform.getPosition().y * zoom) - (offset.y * zoom), 0);
      glRotated(Math.toDegrees(transform.getRotation()), 0, 0, 1);
      glScaled(sprite.getScale(), sprite.getScale(), 1);
      glTranslated((sprite.getOffset().x * zoom), (sprite.getOffset().y * zoom), 0);
      glScaled(zoom / 50, zoom / 50, 1);
      glTranslated((-(texture.getImageWidth() / 2)), (-(texture.getImageHeight() / 2)), 0);
      texture.bind();
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glBegin(GL_QUADS);
      glTexCoord2d(0, 0);
      glVertex2d(0, 0); // upper-left

      glTexCoord2d(0, height);
      glVertex2d(0, texture.getImageHeight()); // upper-right

      glTexCoord2d(width, height);
      glVertex2d(texture.getImageWidth(), texture.getImageHeight()); // bottom-right

      glTexCoord2d(width, 0);
      glVertex2d(texture.getImageWidth(), 0); // bottom-left


      glEnd();
      glPopMatrix();
    }
    catch (IOException e) { e.printStackTrace(); }
  }

  @Override
  public int getLayerOfNext() {
    Sprite sprite = sm.get(layerOrdered.get(index));
    return (int)sprite.getZ();
  }

  @Override
  protected void inserted(Entity e) {
    Sprite sprite = sm.get(e);
    for(int i = 0; i < layerOrdered.size(); i++) {
      Sprite next = sm.get(layerOrdered.get(i));
      if(sprite.getZ() < next.getZ()) {
        layerOrdered.add(i, e);
        return;
      }
    }
    layerOrdered.add(e);
  }

  @Override
  protected void removed(Entity e) {
    layerOrdered.remove(e);
  }

  public void reorder() {
    layerOrdered.clear();
    for(int i = 0; i < getActives().size(); i++)
      inserted(getActives().get(i));
  }

  @Override
  protected boolean checkProcessing() {
    return true;
  }
}
