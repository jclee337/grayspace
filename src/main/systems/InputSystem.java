package main.systems;

import com.artemis.systems.VoidEntitySystem;
import main.EventManager;
import main.events.InputEvent;
import org.lwjgl.input.Keyboard;

public class InputSystem extends VoidEntitySystem {
  EventManager eventManager;
  public InputSystem() {

  }

  @Override
  public void initialize() {
    eventManager = world.getManager(EventManager.class);
  }

  @Override
  protected void processSystem() {
    while(Keyboard.next()) {
      eventManager.broadcast(new InputEvent(Keyboard.getEventKey(), Keyboard.getEventKeyState()));
      //System.out.println(Keyboard.KEY_LEFT);
      //System.out.println(Keyboard.getEventKey());
    }
  }
}
