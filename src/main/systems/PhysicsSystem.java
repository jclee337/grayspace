package main.systems;

import com.artemis.*;
import com.artemis.annotations.Mapper;
import main.events.*;
import main.components.Agent;
import main.components.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;

import java.util.ArrayList;
import java.util.Arrays;

public class PhysicsSystem extends RelayedEntitySystem {
  private World physicsWorld;
  @Mapper ComponentMapper<Agent> pm;
  @Mapper ComponentMapper<Transform> tm;

  public PhysicsSystem() {
    super(Aspect.getAspectForAll(Agent.class, Transform.class),
        new ArrayList<Class<? extends Event>>(Arrays.asList(EntityDiedEvent.class, EntityMovedEvent.class, EntityRotatedEvent.class)));
    physicsWorld = new World(new Vec2(0, 0));
    physicsWorld.setContinuousPhysics(true);
    physicsWorld.setAllowSleep(true);
    physicsWorld.setWarmStarting(true);
  }

  @Override
  protected void begin() {

  }

  @Override
  protected boolean checkProcessing() {
    return true;
  }

  @Override
  public void process(Entity entity) {
    Agent p = pm.get(entity);
    Transform t = tm.get(entity);
    t.setPosition(p.getBody().getPosition().clone());
    t.setRotation(p.getBody().getAngle());
    //p.getBody().setActive(true);
  }

  @Override
  public void afterEvents() {
    //physicsWorld.step(1.0f / 60.0f, 8, 3);
    physicsWorld.step(world.getDelta() / 1e3f, 8, 3);
  }

  @Override
  public void processEvent(Event event) {
    if(event instanceof EntityDiedEvent) {
      Entity entity = ((EntityDiedEvent)event).entity;
      if(pm.has(entity))
        pm.get(entity).getBody().setActive(false);
    } else if(event instanceof EntityMovedEvent) {
      Entity entity = ((EntityMovedEvent) event).entity;
      if(pm.has(entity))
        pm.get(entity).getBody().setTransform(((EntityMovedEvent)event).position, pm.get(entity).getBody().getAngle());
      if(tm.has(entity))
        tm.get(entity).setPosition(((EntityMovedEvent)event).position);
    } else if(event instanceof EntityRotatedEvent) {
      Entity entity = ((EntityRotatedEvent)event).entity;
      if(pm.has(entity))
        pm.get(entity).getBody().setTransform(pm.get(entity).getBody().getPosition(), ((EntityRotatedEvent)event).rotation);
      if(tm.has(entity))
        tm.get(entity).setRotation(((EntityRotatedEvent)event).rotation);
    } else if(event instanceof EntityVelocityEvent) {
      Entity entity = ((EntityVelocityEvent)event).entity;
      if(pm.has(entity))
        pm.get(entity).getBody().setLinearVelocity(((EntityVelocityEvent)event).velocity);
    }
  }

  @Override
  protected void inserted(Entity e) {
    super.removed(e);
    pm.get(e).getBody().setActive(true);
  }

  @Override
  protected void removed(Entity e) {
    super.removed(e);
    pm.get(e).getBody().setActive(false);
  }

  public World getPhysicsWorld() {
    return physicsWorld;
  }
}
