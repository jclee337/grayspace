package main.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import main.components.Spatial;
import main.components.Transform;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Rot;
import org.jbox2d.common.Vec2;

public class ArenaSystem extends EntityProcessingSystem {
  @Mapper ComponentMapper<Transform> tm;
  private Entity arena;

  public ArenaSystem() {
    super(Aspect.getAspectForAll(Transform.class));
  }

  @Override
  protected void process(Entity entity) {
    Transform transform = tm.get(entity);
    arena = world.getManager(TagManager.class).getEntity("ARENA");
    PolygonShape shape = (PolygonShape)arena.getComponent(Spatial.class).getShape();
    if(!shape.testPoint(new org.jbox2d.common.Transform(new Vec2(0, 0), new Rot(0)), transform.getPosition())) {
      entity.disable();
      System.out.println("entity was disabled because it was out of the arena");
    }
  }
}
