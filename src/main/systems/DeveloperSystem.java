package main.systems;

import com.artemis.systems.VoidEntitySystem;
import main.EntityFactory;
import org.lwjgl.input.Keyboard;

public class DeveloperSystem extends VoidEntitySystem {
  public DeveloperSystem() {
    super();
  }

  @Override
  protected void processSystem() {
    if(Keyboard.isKeyDown(Keyboard.KEY_F1))
      EntityFactory.resetEntities(world, world.getSystem(PhysicsSystem.class));
  }

  private abstract class Command {
    public abstract void execute();
  }
}
