package main.systems;

import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import main.EntityFactory;
import main.EventManager;
import main.Util;
import main.PoolManager;
import main.components.Agent;
import main.components.Transform;
import main.components.Weapon;
import main.events.EntityMovedEvent;
import main.events.EntityRotatedEvent;
import org.jbox2d.common.Vec2;
import org.lwjgl.input.Keyboard;

public class PlayerWeaponSystem extends VoidEntitySystem {
  private long now;
  private boolean right;

  public PlayerWeaponSystem() {

  }

  @Override
  protected void initialize() {
    right = true;
    now = 0;
  }

  @Override
  protected void begin() {
    //now = System.currentTimeMillis();
    now += world.getDelta();
  }

  @Override
  protected void processSystem() {
    Entity guns = world.getManager(TagManager.class).getEntity("PLAYER_GUNS");
    if(!guns.isEnabled())
      return;
    float angVel = guns.getComponent(Agent.class).getBody().m_angularVelocity;
    Transform transform = guns.getComponent(Transform.class);
    Weapon weapon = guns.getComponent(Weapon.class);
    if(weapon.getShotAt() + weapon.getDelay() < now) {
      Vec2 direction = Util.direction(transform.getRotation());
      Vec2 position = transform.getPosition().clone();
      position = position.add(direction.mul(0.9f));
      if(right)
        position.addLocal(new Vec2(-direction.y, direction.x).mul(0.40f));
      else
        position.addLocal(new Vec2(direction.y, -direction.x).mul(0.40f));
      right = !right;

      Entity bullet = EntityFactory.getEntity("PLAYER_BULLET", world, world.getSystem(PhysicsSystem.class));
      bullet.enable();
      Agent physics = bullet.getComponent(Agent.class);
      world.getManager(EventManager.class).broadcast(new EntityMovedEvent(bullet, position));
      world.getManager(EventManager.class).broadcast(new EntityRotatedEvent(bullet, (float)transform.getRotation()));
      physics.getBody().setLinearVelocity(new Vec2(0, 0));
      physics.getBody().applyLinearImpulse(direction.mul(
            bullet.getComponent(Agent.class).getThrust().length()), physics.getBody().getWorldCenter());
      physics.getBody().applyLinearImpulse(new Vec2(-direction.y, direction.x).mul(angVel * .001f),
          physics.getBody().getWorldCenter());
      weapon.setShotAt(now);
    }
  }

  @Override
  protected boolean checkProcessing() {
    return Keyboard.isKeyDown(Keyboard.KEY_Z);
  }
}
