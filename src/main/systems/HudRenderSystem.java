package main.systems;

import com.artemis.systems.VoidEntitySystem;
import org.jbox2d.common.Vec2;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import static org.lwjgl.opengl.GL11.*;

public class HudRenderSystem extends VoidEntitySystem {
  private UnicodeFont font;
  private int fps;

  public HudRenderSystem() {
    super();
    try {
      font = new UnicodeFont("HappyKiller.ttf", 18, false, false);
      font.getEffects().add(new ColorEffect(java.awt.Color.BLACK));
      font.addAsciiGlyphs();
      font.loadGlyphs();
    }
    catch (SlickException e) { e.printStackTrace(); }
    fps = 1;
  }

  @Override
  protected void processSystem() {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBindTexture(GL_TEXTURE_2D, 0);
    glColor4d(0.8, 0.8, 0.8, 1);

    drawString(new Vec2(Display.getWidth() / 2, Display.getHeight() - 14), 0, true,
        world.getSystem(ScoreSystem.class).getScore());

    drawString(new Vec2(0, 0), 0, false, "FPS: " + fps);

    glEnd();
    glPopMatrix();
  }

  private void drawString(Vec2 pos, double angle, boolean centered, String str) {
    if(centered)
      pos = new Vec2(pos.x - font.getWidth(str)/2, pos.y - font.getHeight(str)/2);

    font.drawString((float)pos.x, (float)pos.y, str);
  }

  public void setFPS(int fps) {
    this.fps = fps;
  }
}
