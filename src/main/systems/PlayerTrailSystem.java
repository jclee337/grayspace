package main.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.ImmutableBag;
import main.EntityFactory;
import main.EventManager;
import main.components.Transform;
import main.events.EntityMovedEvent;
import main.events.EntityRotatedEvent;
import org.jbox2d.common.Vec2;

public class PlayerTrailSystem extends IntervalEntitySystem {
  Entity player;
  int index;

  public PlayerTrailSystem() {
    super(Aspect.getEmpty(), 150);
  }

  @Override
  public void initialize() {
    index = 0;

  }

  @Override
  public void begin() {
    player = world.getManager(TagManager.class).getEntity("PLAYER");
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entityImmutableBag) {
    if(player == null)
      return;
    Transform transform = player.getComponent(Transform.class);

    ImmutableBag<Entity> trail = world.getManager(GroupManager.class).getEntities("PLAYER_TRAIL");
    if(index == trail.size())
      index = 0;

    //Vec2 position = transform.getPosition().sub(MathUtil.direction(transform.getRotation()).mul(0.3f));
    Vec2 position = transform.getPosition();
    if(trail.size() < 25) {
      Entity entity = EntityFactory.getEntity("PLAYER_TRAIL", world, world.getSystem(PhysicsSystem.class));
      world.getManager(EventManager.class).broadcast(new EntityMovedEvent(entity, position));
      world.getManager(EventManager.class).broadcast(new EntityRotatedEvent(entity, (float)transform.getRotation()));
    } else {
      Entity entity = trail.get(index);
      entity.getComponent(Transform.class).setPosition(position);
      entity.getComponent(Transform.class).setRotation(transform.getRotation());
      //System.out.println(entity.getComponent(Transform.class).getPosition());
      index++;
    }
  }
}
