package main.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.ImmutableBag;
import main.EntityFactory;
import main.PoolManager;
import main.Util;
import main.components.Health;
import main.events.EntityMovedEvent;
import main.EventManager;
import main.components.Spatial;
import main.components.Transform;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

public class EnemySpawnSystem extends IntervalEntitySystem {
  private Entity arena;
  private Entity player;

  public EnemySpawnSystem() {
    super(Aspect.getEmpty(), 5000);
  }

  @Override
  public void begin() {
    arena = world.getManager(TagManager.class).getEntity("ARENA");
    player = world.getManager(TagManager.class).getEntity("PLAYER");
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entityImmutableBag) {
    if(arena == null || player == null)
      return;

    PolygonShape shape = (PolygonShape)arena.getComponent(Spatial.class).getShape();
    Transform playerTransform = player.getComponent(Transform.class);
    Vec2 position = player.getComponent(Transform.class).getPosition();
    int max = 0, prev, next;
    double length = 0;
    for(int i = 0; i < shape.getVertexCount(); i++) {
      if(shape.getVertex(i).sub(position).length() > length) {
        length = shape.getVertex(i).sub(position).length();
        max = i;
      }
    }
    prev = max == 0 ? shape.getVertexCount() - 1 : max - 1;
    next = max == shape.getVertexCount() - 1 ? 0 : max + 1;

    Vec2 o = shape.getVertex(max);
    Vec2 a = shape.getVertex(prev);
    Vec2 b = shape.getVertex(next);
    double range = Math.acos(((a.sub(o).x * b.sub(o).x) + (a.sub(o).y * b.sub(o).y)) / (a.sub(o).length() * b.sub(o).length()));

    double angle = (Math.random() * range) + Math.atan2(a.sub(o).y, a.sub(o).x);
    double radius = 5;
    radius = (Math.random() * (length / 3)) + radius;
    position = new Vec2((float)Math.cos(angle - Math.PI/2), (float)Math.sin(angle - Math.PI/2));
    position = position.mul((float)radius).add(o);

    Entity enemy;
    int rand = (int)Util.random(1, 100);
    if(rand < 15)
      enemy = EntityFactory.getEntity("CLEANER", world, world.getSystem(PhysicsSystem.class));
    else if(rand < 75)
      enemy = EntityFactory.getEntity("POINTER", world, world.getSystem(PhysicsSystem.class));
    else {
      for(int i = 0; i < 40; i++) {
        enemy = EntityFactory.getEntity("BLOCKER", world, world.getSystem(PhysicsSystem.class));
        enemy.enable();
        world.getManager(EventManager.class).broadcast(new EntityMovedEvent(enemy, position));
        position = position.add(Util.direction(i).mul(i*0.05f));
      }
      return;
    }
    EntityFactory.resetComponent(enemy, Health.class);
    enemy.enable();
    //enemy.getComponent(Health.class).setHealth(2);
    world.getManager(EventManager.class).broadcast(new EntityMovedEvent(enemy, position));
  }

  private double mod(double a, double n) {
    return (a % n + n) % n;
  }
}
