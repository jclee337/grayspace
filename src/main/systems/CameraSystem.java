package main.systems;

import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import main.components.Camera;
import org.lwjgl.input.Keyboard;

public class CameraSystem extends VoidEntitySystem {
  private Entity camera;

  public CameraSystem() {

  }

  @Override
  public void begin() {
    camera = world.getManager(TagManager.class).getEntity("CAMERA");
  }

  @Override
  protected void processSystem() {
    if(camera == null)
      return;

    Camera comp = camera.getComponent(Camera.class);
    if(Keyboard.isKeyDown(Keyboard.KEY_F12))
      comp.setZoom(comp.getZoom() + 0.1);
    if(Keyboard.isKeyDown(Keyboard.KEY_F11))
      comp.setZoom(comp.getZoom() - 0.11);
  }
}
