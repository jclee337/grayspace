package main.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import main.events.EntityDamagedEvent;
import main.events.EntityDiedEvent;
import main.events.Event;
import main.EventManager;
import main.components.Health;

import java.util.ArrayList;
import java.util.Arrays;

public class HealthSystem extends RelayedEntitySystem {
  @Mapper ComponentMapper<Health> hm;

  public HealthSystem() {
    super(Aspect.getAspectForAll(Health.class),
        new ArrayList<Class<? extends Event>>(Arrays.asList(EntityDamagedEvent.class)));
  }

  @Override
  public void process(Entity entity) {
    Health health = hm.get(entity);
    if(health.getHealth() <= 0) {
      entity.disable();
      broadcast(new EntityDiedEvent(entity));
    }
  }

  @Override
  public void processEvent(Event event) {
    if(event instanceof EntityDamagedEvent) {
      if(hm.getSafe(((EntityDamagedEvent)event).entity) != null) {
        Entity entity = ((EntityDamagedEvent) event).entity;
        if(hm.get(entity).getHitAt() + hm.get(entity).getInvulnDuration() < System.currentTimeMillis() && !hm.get(entity).isInvincible()) {
          hm.get(entity).mutHealth(-1);
          hm.get(entity).setHitAt(System.currentTimeMillis());
        }
      }
    }
  }

  @Override
  protected boolean checkProcessing() {
    return true;
  }
}
