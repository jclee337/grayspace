package main.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import main.Util;
import main.components.Agent;
import main.components.Steering;
import main.Boid;
import main.components.Transform;
import org.jbox2d.collision.RayCastInput;
import org.jbox2d.collision.RayCastOutput;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.ShapeType;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Fixture;

import java.util.*;

/**
 *  This system defines behaviors for steering and processes
 *  each entity's set behaviors at each game step
 */
public class SteeringSystem extends EntityProcessingSystem {
  @Mapper ComponentMapper<Steering> sm;
  @Mapper ComponentMapper<Agent> pm;
  @Mapper ComponentMapper<Transform> tm;
  private HashMap<String, Behavior> behaviors;

  public SteeringSystem() {
    super(Aspect.getAspectForAll(Steering.class, Agent.class));

    behaviors = new HashMap<String, Behavior>();

    behaviors.put("SEEK", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 distance = target.getBody().getWorldCenter().sub(subject.getBody().getWorldCenter());
        if(distance.length() < boid.distance) {
          double angle = Math.atan2(distance.y, distance.x) + Math.PI;
          double interest = boid.strength / distance.length();
          steering.splitInterest(angle, interest, boid.spread);
        }
      }
    });

    behaviors.put("INTERCEPT", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 distance = getDistance(subject, target);
        if(distance.length() < boid.distance) {
          float t = distance.length() / target.getBody().getLinearVelocity().length();
          if(t < 0.02)
            t = 0;
          Vec2 future = target.getBody().getLinearVelocity().mul(t).add(target.getBody().getWorldCenter());
          distance = future.sub(subject.getBody().getWorldCenter());
          double angle = Math.atan2(distance.y, distance.x) + Math.PI;
          double interest = boid.strength / distance.length();
          steering.splitInterest(angle, interest, boid.spread);
        }
      }
    });

    behaviors.put("AVOID", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 distance = target.getBody().getWorldCenter().sub(subject.getBody().getWorldCenter());
        if(distance.length() < boid.distance) {
          double angle = Math.atan2(distance.y, distance.x) + Math.PI;
          double danger = boid.strength / distance.length();
          steering.splitDanger(angle, danger, boid.spread);
        }
      }
    });

    behaviors.put("FLEE", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 distance = getDistance(subject, target);
        if(distance.length() < boid.distance && distance.length() > 0) {
          double angle = -(Math.atan2(distance.y, -distance.x) - Math.PI);
          double interest = boid.strength / distance.length();
          steering.splitInterest(angle, interest, boid.spread);
        }
      }
    });

    behaviors.put("FORWARD", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        double angle = Util.mod(subject.getBody().getAngle() + (Math.PI / 2), 2 * Math.PI);
        steering.splitInterest(angle, boid.strength, boid.spread);
      }
    });

    behaviors.put("WANDER", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 direction = subject.getBody().getLinearVelocity();
        if(direction.length() == 0)
          direction = new Vec2(Util.random(-1f, 1f), Util.random(-1f, 1f));
        //direction.normalize();
        double angle = Math.atan2(direction.y, direction.x) + (Math.PI);
        angle = Math.random() < 0.5 ?
          Util.mod(angle + Util.random(-1.0, 1.0), 2 * Math.PI) :
          angle;
        steering.splitInterest(angle, boid.strength, boid.spread);
      }
    });

    behaviors.put("ALIGN", new Behavior() {
      @Override
      public void resolve(Steering steering, Agent subject, Agent target, Boid boid) {
        Vec2 distance = target.getBody().getWorldCenter().sub(subject.getBody().getWorldCenter());
        double angle = Math.atan2(distance.y, distance.x);
        double align = Util.mod(angle - subject.getBody().getAngle() + (3 * Math.PI / 2), 2 * Math.PI) - Math.PI;
        steering.placeAlign(align);
      }
    });
  }

  @Override
  protected void process(Entity entity) {
    Steering steering = sm.get(entity);
    Agent physics = pm.get(entity);

    // process each behavior
    for(Map.Entry<String, ArrayList<Boid>> entry : steering.targets.entrySet()) {
      ImmutableBag<Entity> entities = world.getManager(GroupManager.class).getEntities(entry.getKey());
      for(int i = 0; i < entities.size(); i++) {
        List<Boid> list = entry.getValue();
        for(Boid boid : list) {
          if(entity.equals(entities.get(i)) || !entities.get(i).isEnabled())
            continue;
          if(pm.getSafe(entities.get(i)) != null)
            behaviors.get(boid.getBehavior()).resolve(steering, physics, pm.get(entities.get(i)), boid);
        }
      }
    }

    // process the resulting behavior maps
    ArrayList<Integer> indices = new ArrayList<Integer>();
    double minDanger = Double.MAX_VALUE;
    for(int i = 0; i < steering.danger.length; i++) {
      if(steering.danger[i] < minDanger) {
        indices = new ArrayList<Integer>();
        indices.add(i);
        minDanger = steering.danger[i];
      }
      else if(steering.danger[i] == minDanger)
        indices.add(i);
    }

    int max = 0;
    for(int i = 0; i < indices.size(); i++) {
      if(steering.interest[indices.get(i)] > steering.interest[max])
        max = indices.get(i);
    }
    if(steering.interest[max] != 0) {
      int prev = max == 0 ? steering.interest.length - 1 : max - 1;
      int next = max == steering.interest.length - 1 ? 0 : max + 1;
      int max2 = steering.interest[prev] > steering.interest[next] ? prev : next;

      float angle1 = (float)((2 * Math.PI / steering.danger.length) * max - Math.PI);
      float angle2 = (float)((2 * Math.PI / steering.danger.length) * max2 - Math.PI);

      Vec2 v1 = new Vec2((float) Math.cos(angle1), (float) Math.sin(angle1)).mul(steering.interest[max].floatValue());
      Vec2 v2 = new Vec2((float) Math.cos(angle2), (float) Math.sin(angle2)).mul(steering.interest[max2].floatValue());

      v1 = new Vec2((v1.x + v2.x) / 2, (v1.y + v2.y) / 2);
      v1.normalize();
      v1 = v1.mul(physics.getThrust().length());

      physics.getBody().applyForceToCenter(v1);
    }

    if(steering.disalign[0] > steering.disalign[1])
      physics.getBody().applyTorque((float)physics.getSpin());
    else if(steering.disalign[0] < steering.disalign[1])
      physics.getBody().applyTorque(-(float)physics.getSpin());
    else {
      if(steering.align[0] > steering.align[1])
        physics.getBody().applyTorque(-(float)physics.getSpin());
      else if(steering.align[0] < steering.align[1])
        physics.getBody().applyTorque((float)physics.getSpin());
    }

    steering.clearMaps();
  }

  public Vec2 getDistance(Agent subject, Agent target) {
    Fixture sub = subject.getBody().getFixtureList();
    Fixture tar = target.getBody().getFixtureList();
    if(sub == null && tar == null)
      return target.getBody().getWorldCenter().sub(subject.getBody().getWorldCenter());
    if(sub == null)
      return raycast(tar, subject.getBody().getWorldCenter()).negate();
    if(tar == null)
      return raycast(sub, target.getBody().getWorldCenter());
    return raycast(sub, tar);
  }

  public Vec2 raycast(Fixture shape1, Vec2 point1) {
    if(shape1.getType() == ShapeType.EDGE) {
      EdgeShape edge = (EdgeShape)shape1.getShape();
      float l2 = edge.m_vertex2.sub(edge.m_vertex1).lengthSquared();
      if(l2 <= 0)
        return point1.sub(edge.m_vertex1);
      float t = (float) Util.dot(point1.sub(edge.m_vertex1), edge.m_vertex2.sub(edge.m_vertex1)) / l2;
      if(t < 0)
        return point1.sub(edge.m_vertex1);
      else if(t > 1)
        return point1.sub(edge.m_vertex2);
      Vec2 projection = edge.m_vertex2.sub(edge.m_vertex1).mul(t).add(edge.m_vertex1);
      return projection.sub(point1);
    } else {
      RayCastInput input = new RayCastInput();
      RayCastOutput output = new RayCastOutput();
      Vec2 point2 = shape1.getBody().getWorldCenter();

      input.p1.set(point1);
      input.p2.set(point2);
      input.maxFraction = 1;
      shape1.raycast(output, input, 0);
      return point2.sub(point1).mul(output.fraction);
    }
  }

  public Vec2 raycast(Fixture shape1, Fixture shape2) {
    Vec2 p2 = shape1.getBody().getWorldCenter();
    Vec2 p1 = shape2.getBody().getWorldCenter();
    Vec2 dist21 = raycast(shape1, p2);
    float f = p2.sub(p1).length() - dist21.length();
    Vec2 offset = p2.sub(p1);
    offset.normalize();
    offset = offset.mul(f);
    p1 = p1.add(offset);
    return raycast(shape2, p1);
  }

  private abstract class Behavior {
    public abstract void resolve(Steering steering, Agent subject, Agent target, Boid boid);
  }
}
