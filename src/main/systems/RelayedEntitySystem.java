package main.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.ImmutableBag;
import main.events.Event;
import main.EventManager;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class RelayedEntitySystem extends EntitySystem {
  private Queue<Event> events;
  private List<Class<? extends Event>> subjects;

  public RelayedEntitySystem(Aspect aspect, List<Class<? extends Event>> subjects) {
    super(aspect);
    events = new LinkedList<Event>();
    this.subjects = subjects;
  }

  @Override
  public void initialize() {
    world.getManager(EventManager.class).register(this);
  }

  @Override
  public void processEntities(ImmutableBag<Entity> entities) {
    while(!events.isEmpty())
      processEvent(events.remove());

    afterEvents();

    for (int i = 0, s = entities.size(); s > i; i++) {
      process(entities.get(i));
    }

  }

  public void afterEvents() {

  }

  public abstract void process(Entity entity);

  public void addEvent(Event event) {
    events.add(event);
  }

  public List<Class<? extends Event>> getSubjects() {
    return subjects;
  }

  public abstract void processEvent(Event event);

  public void broadcast(Event event) {
    world.getManager(EventManager.class).broadcast(event);
  }
}
