package main.systems;

import com.artemis.Entity;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import main.Boid;
import main.TypeManager;
import main.components.*;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;

public class PlayerControlSystem extends VoidEntitySystem {
  private Entity player;
  private Entity guns;
  private Entity camera;
  private Entity arena;
  private Agent playerBody;
  private Agent gunsBody;
  private float thrust, spin, gunSpin;
  private long cCooldown = 15000, cAt;

  public PlayerControlSystem() {
    super();
  }

  @Override
  public void begin() {
    player = world.getManager(TagManager.class).getEntity("PLAYER");
    guns = world.getManager(TagManager.class).getEntity("PLAYER_GUNS");
    camera = world.getManager(TagManager.class).getEntity("CAMERA");
    arena = world.getManager(TagManager.class).getEntity("ARENA");
    playerBody = player.getComponent(Agent.class);
    gunsBody = guns.getComponent(Agent.class);
  }

  @Override
  public void processSystem() {
    if(player == null || guns == null || camera == null || arena == null || playerBody == null || gunsBody == null)
      return;

    if(!player.isEnabled())
      guns.disable();
    gunsBody.getBody().setTransform(playerBody.getBody().getPosition(), gunsBody.getBody().getAngle());
    gunsBody.getBody().setLinearVelocity(playerBody.getBody().getLinearVelocity());

    boolean down = Keyboard.isKeyDown(Keyboard.KEY_DOWN);
    thrust = down ? playerBody.getThrust().length() * 0.7f : playerBody.getThrust().length();
    spin = down ? (float)playerBody.getSpin() * 0.5f : (float)playerBody.getSpin();
    gunSpin = down ? (float)gunsBody.getSpin() * 0.5f : (float)gunsBody.getSpin();

    if(Keyboard.isKeyDown(Keyboard.KEY_X)) {
      //gunsBody.getBody().setAngularVelocity(0);
      if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))
        gunsBody.getBody().applyAngularImpulse(-gunSpin);
      if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
        gunsBody.getBody().applyAngularImpulse(gunSpin);

      Vec2 vel = new Vec2(playerBody.getBody().getLinearVelocity().x, playerBody.getBody().getLinearVelocity().y);
      vel.normalize();
      playerBody.getBody().applyLinearImpulse(vel.mul(thrust), playerBody.getBody().getWorldCenter());

      float angDif = (float)(mod(Math.atan2(vel.y, vel.x) - playerBody.getBody().getAngle() + (3*Math.PI/2), 2*Math.PI) - Math.PI);
      if(Math.abs(angDif) > Math.PI/32)
        playerBody.getBody().applyTorque(angDif * 2.5f);
    } else {
      if(Keyboard.isKeyDown(Keyboard.KEY_LEFT))
        playerBody.getBody().applyAngularImpulse(-spin);
      if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT))
        playerBody.getBody().applyAngularImpulse(spin);

      float angDif = (float)(mod((playerBody.getBody().getAngle() - gunsBody.getBody().getAngle() + Math.PI), 2*Math.PI) - Math.PI);
      if(Math.abs(angDif) < Math.PI/128) {
        gunsBody.getBody().setTransform(playerBody.getBody().getPosition(), playerBody.getBody().getAngle());
        gunsBody.getBody().setAngularVelocity(playerBody.getBody().getAngularVelocity());
      }
      else
        gunsBody.getBody().setAngularVelocity(angDif * 15 + playerBody.getBody().getAngularVelocity());
      playerBody.getBody().applyLinearImpulse(playerBody.getBody().getWorldVector(new Vec2(0, -thrust)), playerBody.getBody().getWorldCenter());
    }

    if(Keyboard.isKeyDown(Keyboard.KEY_C) && cAt + cCooldown < System.currentTimeMillis()) {
      ArrayList<Integer> shards = (ArrayList<Integer>)world.getManager(TypeManager.class).getEntities("SHARD");
      for(int i = 0; i < shards.size(); i++) {
        Entity shard = world.getEntity(shards.get(i));
        if(shard.isEnabled())
          shard.getComponent(Steering.class).addTarget("PLAYER", new Boid("INTERCEPT", 1, 50, 1));
      }
      cAt = System.currentTimeMillis();
    }

    float zoom = (float)camera.getComponent(Camera.class).getZoom();
    Vec2 playerCenter = new Vec2(
      playerBody.getBody().getPosition().x - ((Display.getWidth() / 2) / zoom), 
      playerBody.getBody().getPosition().y - ((Display.getHeight() / 2) / zoom));
    Vec2 arenaCenter = new Vec2(((PolygonShape)(arena.getComponent(Spatial.class).getShape())).m_centroid.sub(
          new Vec2(((Display.getWidth() / 2) / zoom), ((Display.getHeight() / 2) / zoom))));
    camera.getComponent(Transform.class).setPosition(new Vec2((playerCenter.x + arenaCenter.x) / 2, (playerCenter.y + arenaCenter.y) / 2));

  }

  private double mod(double a, double n) {
    return (a % n + n) % n;
  }
}
