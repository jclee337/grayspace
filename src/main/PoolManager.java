package main;

import com.artemis.Entity;
import com.artemis.Manager;
import com.artemis.managers.GroupManager;

import java.util.HashMap;
import java.util.LinkedList;

/**
 *  This manager is responsible for recycling disabled entities
 */
public class PoolManager extends Manager {
  private HashMap<String, LinkedList<Entity>> disabled;

  public PoolManager() {
    disabled = new HashMap<String, LinkedList<Entity>>();
  }

  @Override
  protected void initialize() {

  }

  public Entity getDisabledEntity(String type) {
    if(disabled.get(type) == null || disabled.get(type).peekFirst() == null)
      return null;
    return disabled.get(type).removeFirst();
  }

  @Override
  public void disabled(Entity entity) {
    String type = world.getManager(TypeManager.class).getType(entity);
    if(disabled.get(type) == null)
      disabled.put(type, new LinkedList<Entity>());
    disabled.get(type).push(entity);
  }

  @Override
  public void deleted(Entity entity) {
    String type = world.getManager(TypeManager.class).getType(entity);
    LinkedList<Entity> byType = disabled.get(type);
    if(byType != null)
      byType.remove(entity);
  }

  public void clear() {
    disabled = new HashMap<String, LinkedList<Entity>>();
  }
}
